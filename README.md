# Ansible jitsi-meet role

## Overview

This is an Ansible role for installing jitsi-meet on Debian or Ubuntu.
On its own it's insufficient and also needs role `jitsi-videobridge` to do
something useful. Role `jitsi-meet` installs nginx, jitsi-meet, prosody
and jicofo. Role `jitsi-videobridge` installs the jitsi videobridge. If
you aren't comfortable with these components, read the "Jitsi
architecture" section below to understand them.

## Dependencies

Make sure you get the roles listed in `requirements.yml`.

## Installation

In order to install Jitsi, you need to setup need to setup inventory,
variables and playbook.

### Inventory

Here is an example inventory for installing both roles on the same
server:

    [jitsi_meet]
    my-jitsi-meet-server

    [jitsi_videobridge]
    my-jitsi-meet-server

The `jitsi_meet` group must contain only one server. That server must
also be listed in the `jitsi_videobridge` group ; that is, the Jitsi
Meet server must also be a videobridge (this is a limitation of the
Jitsi Debian packages, where `jitsi-videobridge` is a dependency of
`jitsi-meet`). Additional videobridges can be added if desired.  The
playbook (see below) will assign the `jitsi-meet` role to the server in
the `jitsi_meet` group and the `jitsi-videobridge` role to the servers
in the `jitsi_videobridge` group.

Although in principle nginx, jitsi-meet, prosody and jicofo could reside
in different machines, the role `jitsi-meet` puts them all in a single
server.

### Variables

You need to make certain that some variables are available to the
roles, e.g. by putting them in `group_vars/all`. Obviously it's a good
idea to vault the passwords/secrets. Here is an example:

    jicofo_password: topsecret1
    jicofo_secret: topsecret2
    videobridge_user: myvideobridgeuser
    videobridge_password: topsecret3
    videobridge_muc_nickname: myvideobridge_muc_nick

### Playbook

The easiest way to install Jitsi is to run the included `playbook.yml`,
or to include it in another playbook like this:

    - import_playbook: roles/jitsi-meet/playbook.yml

## Jitsi architecture

The **Jitsi videobridge**, also known as **jvb**, is the component that
does the most essential work: it hosts conferences. All users connect to
the videobridge; they send their video and audio stream to it, and
receive the video and audio streams of the other users from it. Clients
talk WebRTC to the videobridge on udp port 10000 (this does not appear
to be configurable).

The videobridge also listens on a tcp port, typically 9090, that speaks
"Colibri", a protocol on top of HTTP/WebSocket. Clients receive extra
information from that connection, such as which users connected or
disconnected from the conference. This port is actually proxied through
nginx, so clients actually connect to nginx at 443, and URL
`/colibri-ws/[videobridge-id]/` is proxied to the videobridge.

Although the most common case is for a Jitsi installation to use a
single videobridge, you can have many videobridges for better
performance if you have many conferences, and each videobridge can host
one or more conferences. (A given conference always uses a single
videobridge.)

**Jicofo**, misleadingly meaning "Jitsi conference focus", is the
component that orchestrates conferences. Users connect to jicofo and ask
to create or join a conference. Jicofo creates the conference on the
less loaded videobridge and directs users to it. All communication
between Jicofo and other components goes through prosody (more on that
below). Jicofo also listens to port 8888, but this is only a REST API
for diagnostic information or so.

**Jitsi-meet** is the JavaScript client the user runs on the browser. It
is loaded through nginx when the user visits the jitsi web site.

**Prosody** is a XMPP signaling server that in this case works as a
publish/subscribe queue. Videobridges publish information about
themselves on prosody, and jicofo subscribes to prosody and gets that
information from there. In addition, jitsi meet connects to prosody (via
nginx) and communicates with jicofo through it. Prosody listens on ports
5222 (XMPP client), 5269 (XMPP server), and 5280 (XMPP BOSH). Most
components, such as Jicofo and videobridges, connect to 5222. Nginx
also proxies `/http-bind` and `/xmpp-websocket` to prosody's 5280.

For additional information on Jitsi, see:

- the [Architecture section in the Jitsi
  documentation](https://jitsi.github.io/handbook/docs/architecture/)
- a video on [how to load balance jitsi
  meet](https://www.youtube.com/watch?v=LyGV4uW8km8), which is useful
  even if you don't intend to load-balance
- a [related
  discussion](https://community.jitsi.org/t/architecture-design-of-jicofo/14906/2)
  in the forum, notably the [RENATER
  document](https://conf-ng.jres.org/2015/document_revision_1830.html?download)
  and the [flow
  chart](https://go.gliffy.com/go/publish/image/7649541/L.png).
- another [related
  discussion](https://community.jitsi.org/t/jicofo-and-prosody-ports/119669/1)
  in the forum.

## Variables and options

- `jicofo_password`, `jicofo_secret`: The Jicofo username is set as
  "focus", and the password is set to the value of `jicofo_password`.
  It's not actually used anywhere (but has to be set). Likewise with the
  `jicofo_secret`.
- `videobridge_user`, `videobridge_password`: Username and password for
  the videobridge. The user is registered in prosody, and subsequently
  the videobridges connect to prosody as this user. The user is also
  apparently used for SIP, but this is currently not supported by this
  role.
- `videobridge_muc_nickname`: (Used only by the `jitsi-videobridge`
  role.) Any unique string that is the same for all videobridges will
  work here. Other than that, we don't know exactly what it is for. See
  the [Jitsi multi-user chat
  documentation](https://github.com/jitsi/jitsi-videobridge/blob/master/doc/muc.md)
  for more information.

## Meta

Written by Antonis Christofides. May contain some stuff from
https://github.com/udima-university/ansible-jitsi-meet.

Copyright (C) 2022-2023 GRNET  
Copyright (C) 2020-2022 The copyright holders of ansible-jitsi-meet

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.
